from Nuamp import *



def assignButtonClicks(ui):
    # back button clicked
    ui.cpBackButton.clicked.connect(lambda: ui.stackedWidget.setCurrentIndex(0))
    ui.pdBackButton.clicked.connect(lambda: ui.stackedWidget.setCurrentIndex(1))
    ui.vtmBackButton.clicked.connect(lambda: ui.stackedWidget.setCurrentIndex(2))
    ui.mainMenuBackButton.clicked.connect(lambda: ui.stackedWidget.setCurrentIndex(0))
    ui.settingsBackButton.clicked.connect(lambda: ui.stackedWidget.setCurrentIndex(0))

    # next button clicked
    ui.newRunButton.clicked.connect(lambda: ui.stackedWidget.setCurrentIndex(1))
    ui.cpNextButton.clicked.connect(lambda: ui.stackedWidget.setCurrentIndex(2))
    ui.pdNextButton.clicked.connect(lambda: ui.stackedWidget.setCurrentIndex(3))

    #about button clicked
    ui.aboutButton.clicked.connect(lambda: ui.stackedWidget.setCurrentIndex(8))

    #settings button clicked
    ui.settingsButton.clicked.connect(lambda: ui.stackedWidget.setCurrentIndex(9))

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    ui.stackedWidget.setCurrentIndex(0)
    assignButtonClicks(ui)
    sys.exit(app.exec_())