# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'nuamp.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 480)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.stackedWidget = QtWidgets.QStackedWidget(self.centralwidget)
        self.stackedWidget.setGeometry(QtCore.QRect(0, 0, 800, 480))
        self.stackedWidget.setObjectName("stackedWidget")
        self.mainMenu = QtWidgets.QWidget()
        self.mainMenu.setObjectName("mainMenu")
        self.newRunButton = QtWidgets.QPushButton(self.mainMenu)
        self.newRunButton.setGeometry(QtCore.QRect(80, 220, 93, 28))
        self.newRunButton.setObjectName("newRunButton")
        self.patientReportButton = QtWidgets.QPushButton(self.mainMenu)
        self.patientReportButton.setGeometry(QtCore.QRect(280, 220, 93, 28))
        self.patientReportButton.setObjectName("patientReportButton")
        self.settingsButton = QtWidgets.QPushButton(self.mainMenu)
        self.settingsButton.setGeometry(QtCore.QRect(460, 220, 93, 28))
        self.settingsButton.setObjectName("settingsButton")
        self.helpButton = QtWidgets.QPushButton(self.mainMenu)
        self.helpButton.setGeometry(QtCore.QRect(670, 20, 93, 28))
        self.helpButton.setObjectName("helpButton")
        self.aboutButton = QtWidgets.QPushButton(self.mainMenu)
        self.aboutButton.setGeometry(QtCore.QRect(680, 440, 93, 28))
        self.aboutButton.setObjectName("aboutButton")
        self.mainMenuLabel = QtWidgets.QLabel(self.mainMenu)
        self.mainMenuLabel.setGeometry(QtCore.QRect(330, 70, 81, 16))
        self.mainMenuLabel.setObjectName("mainMenuLabel")
        self.stackedWidget.addWidget(self.mainMenu)
        self.chooseProgram = QtWidgets.QWidget()
        self.chooseProgram.setObjectName("chooseProgram")
        self.cpLabel = QtWidgets.QLabel(self.chooseProgram)
        self.cpLabel.setGeometry(QtCore.QRect(310, 20, 141, 16))
        self.cpLabel.setObjectName("cpLabel")
        self.cpCovid19Button = QtWidgets.QPushButton(self.chooseProgram)
        self.cpCovid19Button.setGeometry(QtCore.QRect(180, 130, 93, 28))
        self.cpCovid19Button.setObjectName("cpCovid19Button")
        self.cpRNAButton = QtWidgets.QPushButton(self.chooseProgram)
        self.cpRNAButton.setGeometry(QtCore.QRect(180, 190, 93, 28))
        self.cpRNAButton.setObjectName("cpRNAButton")
        self.cpDNAButton = QtWidgets.QPushButton(self.chooseProgram)
        self.cpDNAButton.setGeometry(QtCore.QRect(180, 260, 93, 28))
        self.cpDNAButton.setObjectName("cpDNAButton")
        self.emptyButton3 = QtWidgets.QPushButton(self.chooseProgram)
        self.emptyButton3.setGeometry(QtCore.QRect(340, 260, 93, 28))
        self.emptyButton3.setObjectName("emptyButton3")
        self.emptyButton2 = QtWidgets.QPushButton(self.chooseProgram)
        self.emptyButton2.setGeometry(QtCore.QRect(340, 190, 93, 28))
        self.emptyButton2.setObjectName("emptyButton2")
        self.emptyButton1 = QtWidgets.QPushButton(self.chooseProgram)
        self.emptyButton1.setGeometry(QtCore.QRect(340, 130, 93, 28))
        self.emptyButton1.setObjectName("emptyButton1")
        self.emptyButton6 = QtWidgets.QPushButton(self.chooseProgram)
        self.emptyButton6.setGeometry(QtCore.QRect(500, 260, 93, 28))
        self.emptyButton6.setObjectName("emptyButton6")
        self.emptyButton5 = QtWidgets.QPushButton(self.chooseProgram)
        self.emptyButton5.setGeometry(QtCore.QRect(500, 190, 93, 28))
        self.emptyButton5.setObjectName("emptyButton5")
        self.emptyButton4 = QtWidgets.QPushButton(self.chooseProgram)
        self.emptyButton4.setGeometry(QtCore.QRect(500, 130, 93, 28))
        self.emptyButton4.setObjectName("emptyButton4")
        self.cpBackButton = QtWidgets.QPushButton(self.chooseProgram)
        self.cpBackButton.setGeometry(QtCore.QRect(20, 420, 93, 28))
        self.cpBackButton.setObjectName("cpBackButton")
        self.cpNextButton = QtWidgets.QPushButton(self.chooseProgram)
        self.cpNextButton.setGeometry(QtCore.QRect(690, 420, 93, 28))
        self.cpNextButton.setObjectName("cpNextButton")
        self.stackedWidget.addWidget(self.chooseProgram)
        self.patientDetails = QtWidgets.QWidget()
        self.patientDetails.setObjectName("patientDetails")
        self.pdLabel = QtWidgets.QLabel(self.patientDetails)
        self.pdLabel.setGeometry(QtCore.QRect(370, 30, 81, 16))
        self.pdLabel.setObjectName("pdLabel")
        self.pdBackButton = QtWidgets.QPushButton(self.patientDetails)
        self.pdBackButton.setGeometry(QtCore.QRect(60, 420, 93, 28))
        self.pdBackButton.setObjectName("pdBackButton")
        self.pdSkipButton = QtWidgets.QPushButton(self.patientDetails)
        self.pdSkipButton.setGeometry(QtCore.QRect(350, 420, 93, 28))
        self.pdSkipButton.setObjectName("pdSkipButton")
        self.pdNextButton = QtWidgets.QPushButton(self.patientDetails)
        self.pdNextButton.setGeometry(QtCore.QRect(670, 420, 93, 28))
        self.pdNextButton.setObjectName("pdNextButton")
        self.stackedWidget.addWidget(self.patientDetails)
        self.addVTMSamples = QtWidgets.QWidget()
        self.addVTMSamples.setObjectName("addVTMSamples")
        self.vtmLabel = QtWidgets.QLabel(self.addVTMSamples)
        self.vtmLabel.setGeometry(QtCore.QRect(290, 60, 211, 16))
        self.vtmLabel.setObjectName("vtmLabel")
        self.vtmBackButton = QtWidgets.QPushButton(self.addVTMSamples)
        self.vtmBackButton.setGeometry(QtCore.QRect(40, 440, 93, 28))
        self.vtmBackButton.setObjectName("vtmBackButton")
        self.vtmNextButton = QtWidgets.QPushButton(self.addVTMSamples)
        self.vtmNextButton.setGeometry(QtCore.QRect(670, 440, 93, 28))
        self.vtmNextButton.setObjectName("vtmNextButton")
        self.stackedWidget.addWidget(self.addVTMSamples)
        self.placeTipWell = QtWidgets.QWidget()
        self.placeTipWell.setObjectName("placeTipWell")
        self.insertLabel1 = QtWidgets.QLabel(self.placeTipWell)
        self.insertLabel1.setGeometry(QtCore.QRect(250, 60, 341, 16))
        self.insertLabel1.setObjectName("insertLabel1")
        self.insertLabel2 = QtWidgets.QLabel(self.placeTipWell)
        self.insertLabel2.setGeometry(QtCore.QRect(350, 90, 81, 20))
        self.insertLabel2.setObjectName("insertLabel2")
        self.insertBackButton = QtWidgets.QPushButton(self.placeTipWell)
        self.insertBackButton.setGeometry(QtCore.QRect(50, 420, 93, 28))
        self.insertBackButton.setObjectName("insertBackButton")
        self.insertMainMenuBackButton = QtWidgets.QPushButton(self.placeTipWell)
        self.insertMainMenuBackButton.setGeometry(QtCore.QRect(670, 430, 93, 28))
        self.insertMainMenuBackButton.setObjectName("insertMainMenuBackButton")
        self.insertOK = QtWidgets.QPushButton(self.placeTipWell)
        self.insertOK.setGeometry(QtCore.QRect(340, 200, 93, 28))
        self.insertOK.setObjectName("insertOK")
        self.stackedWidget.addWidget(self.placeTipWell)
        self.settingUp = QtWidgets.QWidget()
        self.settingUp.setObjectName("settingUp")
        self.settingUpStopButton = QtWidgets.QPushButton(self.settingUp)
        self.settingUpStopButton.setGeometry(QtCore.QRect(330, 290, 93, 28))
        self.settingUpStopButton.setObjectName("settingUpStopButton")
        self.settingUpProgressBar = QtWidgets.QProgressBar(self.settingUp)
        self.settingUpProgressBar.setGeometry(QtCore.QRect(320, 210, 118, 23))
        self.settingUpProgressBar.setProperty("value", 24)
        self.settingUpProgressBar.setObjectName("settingUpProgressBar")
        self.stackedWidget.addWidget(self.settingUp)
        self.nuampRunning = QtWidgets.QWidget()
        self.nuampRunning.setObjectName("nuampRunning")
        self.pauseButton = QtWidgets.QPushButton(self.nuampRunning)
        self.pauseButton.setGeometry(QtCore.QRect(110, 430, 93, 28))
        self.pauseButton.setObjectName("pauseButton")
        self.stopButton = QtWidgets.QPushButton(self.nuampRunning)
        self.stopButton.setGeometry(QtCore.QRect(590, 430, 93, 28))
        self.stopButton.setObjectName("stopButton")
        self.nrStatusLabel = QtWidgets.QLabel(self.nuampRunning)
        self.nrStatusLabel.setGeometry(QtCore.QRect(380, 430, 55, 16))
        self.nrStatusLabel.setObjectName("nrStatusLabel")
        self.nrLabel = QtWidgets.QLabel(self.nuampRunning)
        self.nrLabel.setGeometry(QtCore.QRect(350, 60, 101, 16))
        self.nrLabel.setObjectName("nrLabel")
        self.stackedWidget.addWidget(self.nuampRunning)
        self.nuampResult = QtWidgets.QWidget()
        self.nuampResult.setObjectName("nuampResult")
        self.nrLabel_2 = QtWidgets.QLabel(self.nuampResult)
        self.nrLabel_2.setGeometry(QtCore.QRect(310, 30, 161, 16))
        self.nrLabel_2.setObjectName("nrLabel_2")
        self.exitButton = QtWidgets.QPushButton(self.nuampResult)
        self.exitButton.setGeometry(QtCore.QRect(120, 410, 93, 28))
        self.exitButton.setObjectName("exitButton")
        self.generatePDFButton = QtWidgets.QPushButton(self.nuampResult)
        self.generatePDFButton.setGeometry(QtCore.QRect(590, 410, 93, 28))
        self.generatePDFButton.setObjectName("generatePDFButton")
        self.stackedWidget.addWidget(self.nuampResult)
        self.about = QtWidgets.QWidget()
        self.about.setObjectName("about")
        self.contactInfoTextBrowser = QtWidgets.QTextBrowser(self.about)
        self.contactInfoTextBrowser.setGeometry(QtCore.QRect(250, 120, 256, 192))
        self.contactInfoTextBrowser.setObjectName("contactInfoTextBrowser")
        self.mainMenuBackButton = QtWidgets.QPushButton(self.about)
        self.mainMenuBackButton.setGeometry(QtCore.QRect(330, 390, 93, 28))
        self.mainMenuBackButton.setObjectName("mainMenuBackButton")
        self.stackedWidget.addWidget(self.about)
        self.settings = QtWidgets.QWidget()
        self.settings.setObjectName("settings")
        self.homeButton = QtWidgets.QPushButton(self.settings)
        self.homeButton.setGeometry(QtCore.QRect(70, 100, 93, 28))
        self.homeButton.setObjectName("homeButton")
        self.homeYButton = QtWidgets.QPushButton(self.settings)
        self.homeYButton.setGeometry(QtCore.QRect(70, 200, 93, 28))
        self.homeYButton.setObjectName("homeYButton")
        self.homeZButton = QtWidgets.QPushButton(self.settings)
        self.homeZButton.setGeometry(QtCore.QRect(70, 300, 93, 28))
        self.homeZButton.setObjectName("homeZButton")
        self.moveY100Button = QtWidgets.QPushButton(self.settings)
        self.moveY100Button.setGeometry(QtCore.QRect(232, 300, 101, 28))
        self.moveY100Button.setObjectName("moveY100Button")
        self.moveY10Button = QtWidgets.QPushButton(self.settings)
        self.moveY10Button.setGeometry(QtCore.QRect(232, 200, 101, 28))
        self.moveY10Button.setObjectName("moveY10Button")
        self.moveY1Button = QtWidgets.QPushButton(self.settings)
        self.moveY1Button.setGeometry(QtCore.QRect(232, 100, 101, 28))
        self.moveY1Button.setObjectName("moveY1Button")
        self.moveZ100Button = QtWidgets.QPushButton(self.settings)
        self.moveZ100Button.setGeometry(QtCore.QRect(392, 300, 101, 28))
        self.moveZ100Button.setObjectName("moveZ100Button")
        self.moveZ10Button = QtWidgets.QPushButton(self.settings)
        self.moveZ10Button.setGeometry(QtCore.QRect(392, 200, 101, 28))
        self.moveZ10Button.setObjectName("moveZ10Button")
        self.moveZ1Button = QtWidgets.QPushButton(self.settings)
        self.moveZ1Button.setGeometry(QtCore.QRect(392, 100, 101, 28))
        self.moveZ1Button.setObjectName("moveZ1Button")
        self.pipetteSpitButton = QtWidgets.QPushButton(self.settings)
        self.pipetteSpitButton.setGeometry(QtCore.QRect(550, 300, 101, 28))
        self.pipetteSpitButton.setObjectName("pipetteSpitButton")
        self.pipetteSuckButton = QtWidgets.QPushButton(self.settings)
        self.pipetteSuckButton.setGeometry(QtCore.QRect(550, 200, 101, 28))
        self.pipetteSuckButton.setObjectName("pipetteSuckButton")
        self.magnetTurnOnButton = QtWidgets.QPushButton(self.settings)
        self.magnetTurnOnButton.setGeometry(QtCore.QRect(550, 100, 101, 28))
        self.magnetTurnOnButton.setObjectName("magnetTurnOnButton")
        self.settingsBackButton = QtWidgets.QPushButton(self.settings)
        self.settingsBackButton.setGeometry(QtCore.QRect(20, 440, 93, 28))
        self.settingsBackButton.setObjectName("settingsBackButton")
        self.stackedWidget.addWidget(self.settings)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        self.stackedWidget.setCurrentIndex(9)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        MainWindow.setTabOrder(self.cpCovid19Button, self.emptyButton4)
        MainWindow.setTabOrder(self.emptyButton4, self.cpBackButton)
        MainWindow.setTabOrder(self.cpBackButton, self.cpNextButton)
        MainWindow.setTabOrder(self.cpNextButton, self.pdBackButton)
        MainWindow.setTabOrder(self.pdBackButton, self.cpRNAButton)
        MainWindow.setTabOrder(self.cpRNAButton, self.cpDNAButton)
        MainWindow.setTabOrder(self.cpDNAButton, self.emptyButton3)
        MainWindow.setTabOrder(self.emptyButton3, self.emptyButton2)
        MainWindow.setTabOrder(self.emptyButton2, self.emptyButton1)
        MainWindow.setTabOrder(self.emptyButton1, self.emptyButton6)
        MainWindow.setTabOrder(self.emptyButton6, self.emptyButton5)
        MainWindow.setTabOrder(self.emptyButton5, self.pdSkipButton)
        MainWindow.setTabOrder(self.pdSkipButton, self.pdNextButton)
        MainWindow.setTabOrder(self.pdNextButton, self.vtmBackButton)
        MainWindow.setTabOrder(self.vtmBackButton, self.vtmNextButton)
        MainWindow.setTabOrder(self.vtmNextButton, self.insertBackButton)
        MainWindow.setTabOrder(self.insertBackButton, self.newRunButton)
        MainWindow.setTabOrder(self.newRunButton, self.patientReportButton)
        MainWindow.setTabOrder(self.patientReportButton, self.settingsButton)
        MainWindow.setTabOrder(self.settingsButton, self.helpButton)
        MainWindow.setTabOrder(self.helpButton, self.aboutButton)
        MainWindow.setTabOrder(self.aboutButton, self.insertMainMenuBackButton)
        MainWindow.setTabOrder(self.insertMainMenuBackButton, self.insertOK)
        MainWindow.setTabOrder(self.insertOK, self.settingUpStopButton)
        MainWindow.setTabOrder(self.settingUpStopButton, self.pauseButton)
        MainWindow.setTabOrder(self.pauseButton, self.stopButton)
        MainWindow.setTabOrder(self.stopButton, self.exitButton)
        MainWindow.setTabOrder(self.exitButton, self.generatePDFButton)
        MainWindow.setTabOrder(self.generatePDFButton, self.contactInfoTextBrowser)
        MainWindow.setTabOrder(self.contactInfoTextBrowser, self.mainMenuBackButton)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.newRunButton.setText(_translate("MainWindow", "New Run"))
        self.patientReportButton.setText(_translate("MainWindow", "Patient Report"))
        self.settingsButton.setText(_translate("MainWindow", "Settings"))
        self.helpButton.setText(_translate("MainWindow", "Help"))
        self.aboutButton.setText(_translate("MainWindow", "About"))
        self.mainMenuLabel.setText(_translate("MainWindow", "Main Menu"))
        self.cpLabel.setText(_translate("MainWindow", "Choose Program to Run"))
        self.cpCovid19Button.setText(_translate("MainWindow", "COVID 19"))
        self.cpRNAButton.setText(_translate("MainWindow", "RNA Isolation"))
        self.cpDNAButton.setText(_translate("MainWindow", "DNA Isolation"))
        self.emptyButton3.setText(_translate("MainWindow", "Empty"))
        self.emptyButton2.setText(_translate("MainWindow", "Empty"))
        self.emptyButton1.setText(_translate("MainWindow", "Empty"))
        self.emptyButton6.setText(_translate("MainWindow", "Empty"))
        self.emptyButton5.setText(_translate("MainWindow", "Empty"))
        self.emptyButton4.setText(_translate("MainWindow", "Empty"))
        self.cpBackButton.setText(_translate("MainWindow", "Back"))
        self.cpNextButton.setText(_translate("MainWindow", "Next"))
        self.pdLabel.setText(_translate("MainWindow", "Patient Details"))
        self.pdBackButton.setText(_translate("MainWindow", "Back"))
        self.pdSkipButton.setText(_translate("MainWindow", "Skip"))
        self.pdNextButton.setText(_translate("MainWindow", "Next"))
        self.vtmLabel.setText(_translate("MainWindow", "Add VTM Samples on the Deep well"))
        self.vtmBackButton.setText(_translate("MainWindow", "Back"))
        self.vtmNextButton.setText(_translate("MainWindow", "Next"))
        self.insertLabel1.setText(_translate("MainWindow", "Insert the Tip Box and Deep Well in the Nuamp "))
        self.insertLabel2.setText(_translate("MainWindow", "and Clicik OK"))
        self.insertBackButton.setText(_translate("MainWindow", "Back"))
        self.insertMainMenuBackButton.setText(_translate("MainWindow", "Main Menu"))
        self.insertOK.setText(_translate("MainWindow", "OK"))
        self.settingUpStopButton.setText(_translate("MainWindow", "Stop"))
        self.pauseButton.setText(_translate("MainWindow", "Pause"))
        self.stopButton.setText(_translate("MainWindow", "Stop"))
        self.nrStatusLabel.setText(_translate("MainWindow", "Message"))
        self.nrLabel.setText(_translate("MainWindow", "Nuamp Running"))
        self.nrLabel_2.setText(_translate("MainWindow", "Nuamp Result (Quick View)"))
        self.exitButton.setText(_translate("MainWindow", "Exit"))
        self.generatePDFButton.setText(_translate("MainWindow", "Generate PDF"))
        self.contactInfoTextBrowser.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:7.8pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Contact Info</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Name:Karthik Ram</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Email:karthikram570@gmail.com</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.mainMenuBackButton.setText(_translate("MainWindow", "Back"))
        self.homeButton.setText(_translate("MainWindow", "Home Device"))
        self.homeYButton.setText(_translate("MainWindow", "Home Y"))
        self.homeZButton.setText(_translate("MainWindow", "Home Z"))
        self.moveY100Button.setText(_translate("MainWindow", "Move y 100 mm"))
        self.moveY10Button.setText(_translate("MainWindow", "Move Y 10 mm"))
        self.moveY1Button.setText(_translate("MainWindow", "Move Y 1 mm"))
        self.moveZ100Button.setText(_translate("MainWindow", "Move Z 100 mm"))
        self.moveZ10Button.setText(_translate("MainWindow", "Move Z 10 mm"))
        self.moveZ1Button.setText(_translate("MainWindow", "Move Z 1 mm"))
        self.pipetteSpitButton.setText(_translate("MainWindow", "Pipette spit"))
        self.pipetteSuckButton.setText(_translate("MainWindow", "Pipette suck"))
        self.magnetTurnOnButton.setText(_translate("MainWindow", "Magnet Turn On"))
        self.settingsBackButton.setText(_translate("MainWindow", "Back"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

